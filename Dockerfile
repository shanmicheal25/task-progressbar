FROM node:12-alpine

RUN apk add --update --no-cache git bash curl zip libexif udev chromium chromium-chromedriver xvfb ttf-freefont

ENV CHROME_BIN chromium-browser