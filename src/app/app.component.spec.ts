import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ProgressbarComponent } from './components/progressbar/progressbar.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ApiService } from './services/bar/api.service';
import { HttpClientModule } from '@angular/common/http';

describe('AppComponent', () => {

  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let h1: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        ReactiveFormsModule,
        HttpClientModule
      ],
      declarations: [
        AppComponent,
        DashboardComponent,
        ProgressbarComponent
      ],
      providers: [ ApiService ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    h1 = fixture.nativeElement.querySelector('h1');
    console.log(h1);
 
    fixture.detectChanges();
  });

  it('should have <h1> with  progressbar', () => {
    const loginElement: HTMLElement = fixture.nativeElement;
     h1 = loginElement.querySelector('h1');
    expect(h1.textContent).toEqual(' Welcome to progressbar! ');
  });

  it('should have <br> ', () => {
    const loginElement: HTMLElement = fixture.nativeElement;
    let br = loginElement.querySelector('br');
    expect(br).toBeTruthy();
  });


  it('should create the app', () => {
    //const app = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  });

  
});
