import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ApiService } from 'src/app/services/bar/api.service';
import { BarDetail } from 'src/app/pojo/bar/bar-detail';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  formDashboard: FormGroup;
  progressBars = [];
  barDetails: BarDetail;

  buttons: any[];
  bars: any[];
  limit: number = 210; // assign default.

  constructor(private formBuilder: FormBuilder, private apiService: ApiService) {
    this.formDashboard = this.formBuilder.group({
      progressBars: [''],
      progressDropDown: ''
    });
  }

  ngOnInit() {
    /**
     * LOAD value from the Bar APi
     * sample : { "buttons": [14, -20],"bars": [44, 32],"limit": 180 }
     */
    this.apiService.getBarDetails().subscribe((res) => {
      console.log('test  ::::: ' + JSON.stringify(res));
      this.barDetails = res;

      this.buttons = this.barDetails.buttons
      this.bars = this.barDetails.bars
      this.limit = this.barDetails.limit

      // help to do ascending order 
      this.buttons = this.buttons.sort((a, b) => (a > b ? 1 : -1));
      console.log('bar value : ' + this.barDetails)

      this.progressBars = []
      this.progressBars = this.getBars();
      console.log('test value : ' + JSON.stringify(this.progressBars))
    });
  }

  /**
   * This method use for create a dropdown list for choose the progressbar 
   */
  getBars() {
    var barsValue = []
    for (let i = 0; i < this.bars.length; i++) {
      barsValue.push({ 'id': i, 'name': 'Progress Bar ' + (i + 1) })
    }
    console.log('bars Value : ' + JSON.stringify(barsValue));
    return barsValue;
  }

  /**
   * drop down list on change value assign.
   * @param e 
   */
  changeProgress(e) {
    console.log(e.value)
    this.progressDropDown.setValue(e.target.value, {
      onlySelf: true
    })
  }

  /**
   * This calculation method help to increase/decrease the progress bar 
   * @param buttonValue 
   */
  calculation(buttonValue) {
    console.log(buttonValue + ' : ' + this.progressDropDown.value)
    this.bars[this.progressDropDown.value] = this.bars[this.progressDropDown.value] + buttonValue
    if (this.bars[this.progressDropDown.value] <= 0) {
      this.bars[this.progressDropDown.value] = 0
    }
    
    // else if (this.bars[this.progressDropDown.value] >= this.limit) {
    //   this.bars[this.progressDropDown.value] = this.limit
    // }
  }

  /**
   * on progress bar on click assign value to progress bar dropdown list.
   */
  progressbarClick(index){
    console.log('test : '+index)
    this.progressDropDown.setValue(index, {
      onlySelf: true
    })
  }

  /**
   * read the drop down list value.
   */
  get progressDropDown() {
    return this.formDashboard.get('progressDropDown');
  }

}
