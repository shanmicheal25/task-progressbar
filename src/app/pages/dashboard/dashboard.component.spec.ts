import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { browser, element, by} from 'protractor'

import { DashboardComponent } from './dashboard.component';
import { ProgressbarComponent } from 'src/app/components/progressbar/progressbar.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ApiService } from 'src/app/services/bar/api.service';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;

  let element: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule,
        HttpClientModule],
      declarations: [
        DashboardComponent,
        ProgressbarComponent],
      providers: [ApiService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should display the `Choose your progress bar` drop down', () => {
    //There should a create drop down in list
    const select: HTMLSelectElement = fixture.debugElement.query(By.css('#progressBars')).nativeElement;
    console.log('select -----: '+select.options.length)
    //select.value = select.options[3].value;  // <-- select a new value
    select.dispatchEvent(new Event('change'));
    console.log('select -----: '+select.options.length)
    fixture.detectChanges();
  });


});
