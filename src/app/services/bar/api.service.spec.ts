
import { TestBed, getTestBed } from '@angular/core/testing';
import { ApiService } from './api.service';

// Http testing module and mocking controller
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { BarDetail } from 'src/app/pojo/bar/bar-detail';

describe('ApiService', () => {

  let injector: TestBed;
  let service: ApiService;

  let httpMock: HttpTestingController;
  let barDetail: BarDetail

  beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ApiService]
    });

    // Inject the http service and test controller for each test
    injector = getTestBed();
    service = injector.get(ApiService);
    httpMock = injector.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    const service: ApiService = TestBed.get(ApiService);
    expect(service).toBeTruthy();
  });

  describe('#getBarDetail', () => {
    it('should return an Observable<BarDetails>', () => {
      service.getBarDetails().subscribe(barDetail => {
        expect(barDetail.bars).not.toBe(null);
      });
      const req = httpMock.expectOne(`${service.apiURL}/bars`);
      expect(req.request.method).toBe("GET");
    });

    it('should do something async', (done) => {
      setTimeout(() => {
        expect(true).toBe(true);
        done();
      }, 2000);
    });
  });

  

});
