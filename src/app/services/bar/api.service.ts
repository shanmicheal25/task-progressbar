import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BarDetail } from 'src/app/pojo/bar/bar-detail';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  apiURL: string = 'http://pb-api.herokuapp.com';
  constructor(private httpClient: HttpClient) { }

  /**
   * This method used for get the bar details from the api url.
   */
  public getBarDetails() {
    return this.httpClient.get<BarDetail>(`${this.apiURL}/bars`);
  }
}
