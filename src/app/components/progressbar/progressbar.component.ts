import { Component, OnInit, Input, OnChanges  } from '@angular/core';

@Component({
  selector: 'app-progressbar',
  templateUrl: './progressbar.component.html',
  styleUrls: ['./progressbar.component.css']
})
export class ProgressbarComponent   {

  @Input() widthVal: number;
  @Input() limitVal: number;

}
